<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//login
Route::POST('logout', '\App\Http\Controllers\API\AuthController@logout');
Route::POST('login', '\App\Http\Controllers\API\AuthController@actionlogin');
Route::POST('register', '\App\Http\Controllers\API\RegisterController@actionregister');

// Post crud
Route::GET('posts', '\App\Http\Controllers\PostController@index');
Route::GET('posts/{id}', '\App\Http\Controllers\PostController@show');
Route::POST('posts', '\App\Http\Controllers\PostController@store');
Route::PUT('posts/{id}', '\App\Http\Controllers\PostController@update');
Route::DELETE('posts/{id}', '\App\Http\Controllers\PostController@destroy');
