<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::all();
        return response()->json([
            'data' => $post,
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'author' => 'required',
            'status' => 'required',
        ]);
        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'author' => $request->author,
            'status' => $request->status,
        ]);
        return response()->json([
            'data' => $post,
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }


    public function update(Request $request, string $id)
    {
        $post = Post::find($id);
        $post->update($request->all());
        return response()->json([
            'data' => $post,
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }

    // public function update(Request $request)
    // {
    //     // dd($request);
    //     $request->validate([
    //         'title' => 'required',
    //         'content' => 'required',
    //         'author' => 'required',
    //         'status' => 'required',
    //     ]);
    //     $post = Post::where('id', $request->id)
    //         ->update([
    //             'title' => $request->title,
    //             'content' => $request->content,
    //             'author' => $request->author,
    //             'status' => $request->status,
    //         ]);


    //     return response()->json([
    //         'data' => $post,
    //         'success' => 1,
    //         'message' => 'Data success',
    //     ], 200);
    // }

    public function destroy($id)
    {
        Post::where('id', $id)
            ->delete();
        return response()->json([
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }

    public function show($id)
    {
        $post = Post::find($id);
        return response()->json([
            'data' => $post,
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return response()->json([
            'data' => $post,
            'success' => 1,
            'message' => 'Data success',
        ], 200);
    }
}
