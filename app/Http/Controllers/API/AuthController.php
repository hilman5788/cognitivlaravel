<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AuthController extends Controller
{

    public function actionlogin(Request $request)
    {
        // dd('halooo');
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json([

                'success' => 1,
                'message' => 'Data success',
            ], 200);
        } else {
            return response()->json([

                'success' => 0,
                'message' => 'Wrong Email or Password',
            ], 404);
        }
    }
    public function logout(Request $request)
    {
        return response()->json([

            'success' => 1,
            'message' => 'Data logout',
        ], 200);
    }
}
